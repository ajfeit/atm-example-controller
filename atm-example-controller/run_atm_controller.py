import ATMController as ac
import TestBank as tb

if __name__ == "__main__":
  # test bank interface, authentication is always true for now
  bank = tb.TestBank(lambda: True)
  # setup initial bank account holders and account:
  bank.addUser({
    'name': 'Andrew',
    'cardNumber': '1111',
    'PINverifier': lambda pin: pin=='1234',
    'accounts': []
  })
  
  bank.addUser({
    'name': 'Frank',
    'cardNumber': '2222',
    'PINverifier': lambda pin: pin=='2345',
    'accounts': []
  })
  
  bank.addAccount(
    'Andrew',
    '1111',
    {
      'acctNumber': '5434',
      'balance': 100
    }
  )
  
  bank.addAccount(
    'Andrew',
    '1111',
    {
      'acctNumber': '5435',
      'balance': 200
    }
  )
  
  bank.addAccount(
    'Frank',
    '2222',
    {
      'acctNumber': '5436',
      'balance': 150
    }
  )
  
  # ATM controller
  atm = ac.ATMController(bank)
  
  atm.insertCard()
  
  atm.getAction()