class TestBank():
  def __init__(self, masterAuthentication):
    self.userData = []
    self.masterAuthentication = masterAuthentication
    self.transactions = []
    
  def addUser(self, userData):
    if self.masterAuthentication():
      self.userData.append(userData)
      return True
    return False
    
  def addAccount(self, name, cardNumber, accountData):
    user = self.findUser(name, cardNumber)
    if self.masterAuthentication() and user is not None:
      user['accounts'].append(accountData)
      return True
    return False
    
  def getBalance(self, name, number, account, pin):
    user = self.findUser(name, number)
    if user and user['PINverifier'](pin):
      ai = [i for i,ai in enumerate(user['accounts']) if ai['acctNumber'] == account]
      if ai:
        return user['accounts'][ai[0]]['balance'], True
      else:
        return 0, False
  
  def withdrawel(self, name, number, account, pin, amount):
    user = self.findUser(name, number)
    valid = False
    if user and user['PINverifier'](pin):
      acc = self.findAccount(user, account)
      balance = acc['balance']
      if acc:
        if acc['balance'] >= amount:
          self.transactions.append({
            'type': 'cash_withdrawel',
            'account': acc['acctNumber'],
            'name': name,
            'amount': amount
          })
          acc['balance'] -= amount
          valid = True
          balance = acc['balance']
    return balance, valid
      
  def deposit(self, name, number, account, pin, amount, source):
    user = self.findUser(name, number)
    valid = False
    balance = None
    if user and user['PINverifier'](pin):
      acc = self.findAccount(user, account)
      if acc:
        self.transactions.append({
          'type': 'check_deposit',
          'account': acc['acctNumber'],
          'name': name,
          'amount': amount,
          'source': source
        })
        acc['balance'] += amount
        valid = True
        balance = acc['balance']
    return balance, valid
  
  def findUser(self, name, cardNumber):
    res = [i for i,u in enumerate(self.userData) if (u['name'] == name) and (u['cardNumber'] == cardNumber)]
    if not res:
      print('Cannot find user', name)
      return
    else:
      return self.userData[res[0]]
    
  def findAccount(self, user, account):
    ai = [i for i,ai in enumerate(user['accounts']) if ai['acctNumber'] == account]
    if ai:
      return user['accounts'][ai[0]]
    else:
      return None