
class ATMController():
  def __init__(self, bankInterface):
    self.bank = bankInterface
    self.currentCard = None
    
  def insertCard(self, cardNumber=None, cardName=None):
    if self.currentCard is not None:
      print("Card already inserted")
      return
    if cardName is None:
      print('Inserting ATM card...')
      cardName = input('Enter cardholder name: ')
    if cardNumber is None:
      cardNumber = input('Enter card number (XXXX): ')
      # check carNumber validity (length, characters)
    self.currentCard = {
      'number': cardNumber,
      'name': cardName
    }
    print('Card inserted for', self.currentCard['name'])
      
  def removeCard(self):
    print('Removing card for', self.currentCard['name'])
    self.currentCard = None
    self.authenticated = False
    print('No card.')
    
  def getAction(self):
    while True:
      print('Select action:')
      print('1) - check account balance')
      print('2) - withdraw cash')
      print('3) - make a deposit')
      print('4) - return card')
      option = int(input(": "))
      if option == 1:
        self.checkBalance()
      if option == 2:
        self.getWithdrawel()
      if option == 3:
        self.makeDeposit()
      if option == 4:
        self.removeCard()
        break
      if option > 4 or option < 1:
        print('Invalid option.')
    return
  
  def getPin(self):
    account = input("Enter acct #: ")
    pin = input("Enter PIN: ")
    return account, pin

  def checkBalance(self):
    account, pin = self.getPin()
    balance, valid = self.bank.getBalance(self.currentCard['name'], self.currentCard['number'], account, pin)
    if valid:
      print("Your balance is: ", balance)
    else:
      print("Invalid crendentials")
  
  def getWithdrawel(self):
    account, pin = self.getPin()
    amount = int(input('Enter amount to withdraw: '))
    balance, valid = self.bank.withdrawel(self.currentCard['name'], self.currentCard['number'], account, pin, amount)
    if valid:
      self.dispenseCash(amount)
    else:
      print('Could not complete withdrawel.')
    print('Balance: ', balance)
  
  def makeDeposit(self):
    account, pin = self.getPin()
    print('Enter check information')
    amount = int(input('check amount: '))
    depRouting = input('routing #: ')
    depAccount = input('acct #: ')
    depSource = {
      'type': 'check',
      'depRouting': depRouting,
      'depSource': depAccount
    }
    balance, valid = self.bank.deposit(self.currentCard['name'], self.currentCard['number'], account, pin, amount, depSource)
    if not valid:
      print('Could not complete deposit.')
    print('Balance:', balance)

  def dispenseCash(self, amount):
    print('Dispensing: ', amount)
  