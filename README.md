# ATM Ecample Controller
## Running
1. `git clone https://gitlab.com/ajfeit/atm-example-controller.git`
2. `cd atm-example-controller`
3. `python3 ./atm-example-controller/run_atm_controller.py`

Note: the package is installable for use with other applications, but installation is not required to run this example.

## Testing
The file `run_atm_controller.py` initializes two uses, with various accounts, and begins a typical ATM use scenario, allowing the user interface functions to be tested.

The primary requirement of the `ATMController` package is to take user input, and call the correct Bank interface functions. Individual ATMController functions can be tested by callng the associated function (`checkBalance()`, `getWithdrawel()`, `makeDeposit()`) separate from the main ATM run loop (`getAction()`). Modifications would be required to automate testing of a large number of user interface input cases.

## Example Test Procedure:
```
Inserting ATM card...
Enter cardholder name: Andrew
Enter card number (XXXX): 1111
Card inserted for Andrew
Select action:
1) - check account balance
2) - withdraw cash
3) - make a deposit
4) - return card
: 1
Enter acct #: 5434
Enter PIN: 1234
Your balance is:  100
Select action:
1) - check account balance
2) - withdraw cash
3) - make a deposit
4) - return card
: 2
Enter acct #: 5434
Enter PIN: 1234
Enter amount to withdraw: 20
Dispensing:  20
Balance:  80
Select action:
1) - check account balance
2) - withdraw cash
3) - make a deposit
4) - return card
: 3
Enter acct #: 5434
Enter PIN: 1234
Enter check information
check amount: 40
routing #: 5555
acct #: 6666
Balance: 120
Select action:
1) - check account balance
2) - withdraw cash
3) - make a deposit
4) - return card
: 4
Removing card for Andrew
No card.
```

## ATM Actions
ATM Actions request information from the user on the command line, report information, and call the required Bank API methods to complete each request.

- Insert card  `ATMController.inserCard(cardNumber, cardName)`
    - Get card name and card number from user, as if they had inserted a card.
- Remove card  `ATMController.removeCard()`
    - Clear card data if present
- Get Action  `ATMController.getAction()`
    - Request the user for the action they would like to take: check balance, withdraw cash, make a deposit, or remove their card.
- Request Balance  `ATMController.checkBalance()`
    - Get the account number of the user to check, and the user's PIN
    - Make request to bank interface
    - Recieve balance if request is permitted
- Request withdrawel  `ATMController.getWithdrawel()`
    - Get account number of the user to withdraw from, and the user's PIN
    - Get the amount the user wishes to withdraw
    - Make a request to withdraw to bank interface with:
    - Recieve approval/denial for withdrawel from the bank
    - If approved, dispense the cash
- Deposit  `ATMController.makeDeposit()`
    - Get account number of the user and the user's PIN
    - Get the deposit check information (routing, account #, and amount)
    - Make a reqest to deposit to the bank interface.
    - If approced, report the new account balance.

## Bank - Basic Interface for Testing:
- Initialize   `TestBank(masterAuthentication)`
    - `masterAuthentication` is a function that returns true if internal functions, such as adding users or adding accounts is permitted. In a real bank this function would be linked to some other authentication system, or verifications such as for ID, or that funds are available.
- Add a user  `TestBank.addUser(userData)`
    - Requires master authentication
    - `userData` contains:
        - name
        - card number
        - PIN function
    - The PIN function takes in a PIN number, and returns True if the PIN is correct.

- Add account  `TestBank.addAccount(name, cardNumber, accountData)`
    - requires master authentications
    - find the record of the usere corresponding to the name and card number
    - Add the specified account to their list of accounts
    - accountData includes:
        - account number
        - opening balance
- Get account balance  `TestBank.getBalance(name, cardNumber, accountNumber, pin)`
    - finds the user record associated with the name and card number
    - if the PIN is valid:
    - finds the user's account associated with the account number
    - returns balance and validity flag
- Withdraw  `TestBank.withdrawel(name, cardNumber, accountNumber, pin, amount)`
    - finds the user record associated with the name and card number
    - if the PIN is valid:
    - finds the user's account associated with the account number
    - if the account balance is greater-than or equal to the amount requested:
        - Add this transaction to the register
        - Update the account blance
        - return the new balance, and transation validity flag
- Deposit  `TestBank.deposit(name, cardNumber, accountNumber, pin, amount, source)`
    - finds the user record associated with the name and card number
    - if the PIN is valid:
    - finds the user's account associated with the account number
    - Log this transaction, where `source` would likely contain information about a check, such as routing and account number.
    - Update account balance
    - return approval/denial
