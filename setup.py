from setuptools import setup

setup(name='atm-example-controler',
      version='0.1',
      description='ATM Controller class example',
      url='http://gitlab.com/ajfeit/atm-example-controller',
      author='Andrew Feit',
      author_email='feit.andrew.j@gmail.com',
      license='MIT',
      packages=['atm-example-controller'],
      zip_safe=False)